package testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

public class Agegate extends BaseTest
{
	
    
    @Test(priority=1)
	public void open_agegatepage()
	{			
    	driver.navigate().to("https://staging.hkn2.brandcommerce.io");
		test.log(LogStatus.INFO, "Redirected to agegate page");
		test.log(LogStatus.INFO, "we are in agegate page");
		test.log(LogStatus.PASS, "finishing Opening agegate page");		
	}
    
    @Test(priority=2,dependsOnMethods= {"open_agegatepage"})
	public void verify_AgegatePage_title()
	{
		
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "got the title of agegate page");
		String exptitle ="Heineken | Age Gate";
		Assert.assertEquals(acttitle, exptitle);
		test.log(LogStatus.INFO, "verified the title of agegate page");
		System.out.println("verified agegate page title");
	}
    @Test(priority=3,dependsOnMethods= {"open_agegatepage"})
	public void verify_AgegatePage_Logo()
	{
		
    	test.log(LogStatus.INFO, "verifying if logo is present or not in agegate page");
		WebElement logo = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/div/img"));
		Assert.assertTrue(logo.isDisplayed());
		System.out.println("verified logo in agegate page");		
	}
    @Test(priority=4,dependsOnMethods= {"open_agegatepage"})
	public void click_On_TermsOfUse()
	{
    	
    	test.log(LogStatus.INFO, "Click on the terms of use link");
		WebElement terms = driver.findElement(By.xpath("html/body/div[1]/div/div/div/div[1]/div/div/form/div[2]/p/a[1]"));
		Assert.assertTrue(terms.isDisplayed());
		terms.click();
		test.log(LogStatus.INFO, "Clicked on the terms of use link");
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    @Test(priority=5,dependsOnMethods= {"open_agegatepage","click_On_TermsOfUse"})
	public void verify_TermsOfUsePage()
	{
    	
    	test.log(LogStatus.INFO, "We are in terms of use page");
		WebElement termstext = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[1]/ul/li[1]/a"));
		Assert.assertTrue(termstext.isDisplayed());
		test.log(LogStatus.INFO, "Displayed terms and condition title");
	}
    
    @Test(priority=6,dependsOnMethods= {"open_agegatepage","click_On_TermsOfUse"})
	public void verify_title_of_TermsOfUsePage()
	{
    	
    	String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "got the title of terms of use page");
		String exptitle ="Heineken | Read the small print";
		Assert.assertEquals(acttitle, exptitle);
		test.log(LogStatus.INFO, "verified the title of terms of use page");
		System.out.println("verified terms of use page title");
	}
    
    @Test(priority=7,dependsOnMethods= {"open_agegatepage","click_On_TermsOfUse","verify_AgegatePage_title"})
	public void goBack_To_AgegatePage_fromTerms()
	{
		test.log(LogStatus.INFO, "going backto agegate page");
		driver.navigate().back();
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify_AgegatePage_title();
		System.out.println("redirected to agegate page");
	}
    @Test(priority=8,dependsOnMethods= {"open_agegatepage","goBack_To_AgegatePage_fromTerms"})
	public void click_On_Privacy_Policy()
	{   	
    	test.log(LogStatus.INFO, "Checking whether the privacy policy link is available");
		WebElement privacy = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[2]/p/a[2]"));
		Assert.assertTrue(privacy.isDisplayed());
		test.log(LogStatus.INFO, "privacy policy link is available");
		privacy.click();
		test.log(LogStatus.INFO, "Clicked on the privacy policy link");
		System.out.println("Clicked on the privacy policy link");
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    @Test(priority=9,dependsOnMethods= {"open_agegatepage","click_On_Privacy_Policy"})
	public void verify_Privacy_PolicyPage()
	{
    	
    	test.log(LogStatus.INFO, "We are in privacy policy page");
		WebElement privacytext = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[1]/ul/li[2]/a"));
		Assert.assertTrue(privacytext.isDisplayed());
		test.log(LogStatus.INFO, "Displayed privacy policy title");
		System.out.println("verified privacy policy page");
	}
    
    @Test(priority=10,dependsOnMethods= {"open_agegatepage","click_On_Privacy_Policy"})
	public void verify_title_of_Privacy_PolicyPage()
	{
    	
    	String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "got the title of privacy policy page");
		String exptitle ="Heineken | Read the small print";
		Assert.assertEquals(acttitle, exptitle);
		test.log(LogStatus.INFO, "verified the title of privacy policy page");
		System.out.println("verified privacy policy page title");
	}
    
    @Test(priority=11,dependsOnMethods= {"open_agegatepage","click_On_Privacy_Policy"})
	public void back_To_AgegatePage_fromPrivacy()
	{
		test.log(LogStatus.INFO, "going backto agegate page");
		driver.navigate().back();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify_AgegatePage_title();
		System.out.println("redirected to agegate page from privacy policy");
	}
    @Test(priority=12,dependsOnMethods= {"open_agegatepage","back_To_AgegatePage_fromPrivacy"})
	public void check_Copyright_Text()
	{
    	test.log(LogStatus.INFO, "Getting the copyright text");
    	String actual_copyrighttext;
    	actual_copyrighttext = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/div")).getText();
    	System.out.println(actual_copyrighttext);
    	String expected_copyrighttext = "� 2017 HEINEKEN LAGER BEER. IMPORTED BY HEINEKEN USA, WHITE PLAINS, NY";
    	Assert.assertEquals(actual_copyrighttext, expected_copyrighttext);
    	test.log(LogStatus.INFO, "verified the copyright text");
    	System.out.println("verified privacy policy page title");
	}
    @Test(priority=13,dependsOnMethods= {"open_agegatepage","back_To_AgegatePage_fromPrivacy"})
	public void click_OnEnjoy_Responsibility()
	{
    	test.log(LogStatus.INFO, "Getting the enjoy responsibility logo");
    	WebElement enjoylogo;
    	enjoylogo = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/ul/li[1]/a/img"));
    	Assert.assertTrue(enjoylogo.isDisplayed());
    	test.log(LogStatus.INFO, "enjoy responsibility logo is available");
    	enjoylogo.click();
    	test.log(LogStatus.INFO, "clicked on enjoy responsibility logo");
    	System.out.println("clicked on enjoy responsibility logo");
	}
    @Test(priority=14,dependsOnMethods= {"open_agegatepage","click_OnEnjoy_Responsibility"})
    public void verify_Enjoy_Responsibility()
    {
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	test.log(LogStatus.INFO, "verifying the enjoy responsibility page with heineken logo");
    	WebElement heinekenlogo;
    	heinekenlogo = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/img"));
    	Assert.assertTrue(heinekenlogo.isDisplayed());
    	test.log(LogStatus.INFO, "heineken logo is available");
    	  	
    	test.log(LogStatus.INFO, "verifying whether the facebook login button is avaialable");
    	WebElement fbloginbtn;
    	fbloginbtn = driver.findElement(By.xpath("/html/body/div/div[1]/div/form[2]/div/a/span[1]"));
    	Assert.assertTrue(fbloginbtn.isDisplayed());
    	test.log(LogStatus.INFO, "facebook login button is available");  
    	System.out.println("Verified enjoy responsibility page");
    }
    
    @Test(priority=15,dependsOnMethods= {"open_agegatepage","verify_Enjoy_Responsibility"})
	public void back_To_AgegatePage_fromEnjoyResponsibility()
	{
		test.log(LogStatus.INFO, "going backto agegate page");
		driver.navigate().back();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify_AgegatePage_title();
		System.out.println("redirected to agegate page from enjoy responsibility page");
	}
    
    @Test(priority=16,dependsOnMethods= {"open_agegatepage","back_To_AgegatePage_fromEnjoyResponsibility"})
	public void click_verify_Companyinfo_Page()
	{
    	WebElement companyinfolink;
    	
		test.log(LogStatus.INFO, "going to click on company information link");
		companyinfolink = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/ul/li[2]/a"));
		Assert.assertTrue(companyinfolink.isDisplayed());
		companyinfolink.click();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		//switch to new tab
		driver.switchTo().window(browserTabs .get(1));
		test.log(LogStatus.INFO, "switched to new tab");
		//check is it correct page opened or not (e.g. check page's title)
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, "getting the title of company information tab");
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title");
		String Expectedtitle = "The HEINEKEN Company - Age Gate";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of comapny information tab");
		//then close tab and get back
		driver.close();
		test.log(LogStatus.INFO, "Closed the company information tab");
		driver.switchTo().window(browserTabs.get(0));
		test.log(LogStatus.INFO, "Redirected to agegate page again");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("redirected to agegate page from company onformation page");
	}
    @Test(priority=17,dependsOnMethods= {"open_agegatepage","click_verify_Companyinfo_Page"})
    public void age_with_invalid_mm()
    {
    	WebElement error;
    	String errortext;
    	String experror = "Sorry, Inavalid date entered.Please enter a valid date.";
    	test.log(LogStatus.INFO, "providing month as 00");
    	dob_checking(00,01,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified month with 00");	
    	
    	test.log(LogStatus.INFO, "providing month as 00");
    	dob_checking(13,01,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified month with 13");	
    }
    
    @Test(priority=18,dependsOnMethods= {"open_agegatepage","click_verify_Companyinfo_Page"})
    public void age_with_invalid_dd()
    {
    	WebElement error;
    	String errortext;
    	String experror = "Sorry, Invalid date entered.Please enter a valid date.";
    	test.log(LogStatus.INFO, "providing date as 00");
    	dob_checking(01,00,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with 00");	
    	
    	test.log(LogStatus.INFO, "providing date as 32");
    	dob_checking(01,32,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with 13");
    	
    	test.log(LogStatus.INFO, "providing date as April 31");
    	dob_checking(04,31,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with april 31");
    	
    	test.log(LogStatus.INFO, "providing date as feb 30 for leap year");
    	dob_checking(02,30,1988);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with feb 30 for leap year");
    	
    	test.log(LogStatus.INFO, "providing date as feb 29 for non leap year");
    	dob_checking(02,29,1987);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with feb 29 for non leap year");  	
    }
    
    @Test(priority=19,dependsOnMethods= {"open_agegatepage","click_verify_Companyinfo_Page"})
    public void age_with_invalid_yyyy()
    {
    	WebElement error;
    	String errortext;
    	String experror = "Sorry, Invalid date entered.Please enter a valid date.";
    	test.log(LogStatus.INFO, "providing year as 0000");
    	dob_checking(01,01,0000);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with 00");	
    	
    	test.log(LogStatus.INFO, "providing year as upcoming year");
    	dob_checking(01,01,2020);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified date with 13");
    }
    
    @Test(priority=20,dependsOnMethods= {"open_agegatepage","click_verify_Companyinfo_Page"})
    public void age_less_21()
    {
    	WebElement error;
    	String errortext;
    	String experror = "Sorry, come back when you�re of legal drinking age. We�ll keep the Heineken cold.";
    	test.log(LogStatus.INFO, "Providing DOB below 21 years");
    	dob_checking(01,01,2011);
    	error = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/form/div[4]/span"));
    	errortext = error.getText();
    	Assert.assertEquals(errortext, experror);
    	System.out.println("verified age less than 21");					
    }
   
    @Test(priority=21,dependsOnMethods= {"open_agegatepage","click_verify_Companyinfo_Page"})
    public void valid_Age()
    {
    	test.log(LogStatus.INFO, "Providing DOB above 21 years");
    	dob_checking(12,12,1988);
    	test.log(LogStatus.INFO, "Checking whether we have reached the home page");
    	String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title of home page");
		String Expectedtitle = "Heineken Store | Home";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of home page");		
    	System.out.println("verified age above 21 and redirected to home page");		
    }
    
    @Test(priority=21,dependsOnMethods= {"open_agegatepage","valid_Age"})
    public void verify_DOB_in_registrationform()
    {
    	WebElement joinloginbtn;
    	test.log(LogStatus.INFO, "Clicked on join/Login button");
    	joinloginbtn = driver.findElement(By.xpath("/html/body/div[1]/main-navbar/div[2]/nav[1]/header/div/div/div[1]/div[2]/a"));
        joinloginbtn.click();
        test.log(LogStatus.INFO, "pop up the login window");
        try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        WebElement notamembertext;						
  	  notamembertext = driver.findElement(By.cssSelector("#LoginForm > div:nth-child(6) > p:nth-child(2)"));
  	  Assert.assertTrue(notamembertext.isDisplayed());
  	  test.log(LogStatus.INFO, "Not a member yet text is displayed");
  	
  	  WebElement joinnowbtn;
  	  joinnowbtn = driver.findElement(By.cssSelector("#LoginForm > div:nth-child(6) > a:nth-child(3)"));
  	  Assert.assertTrue(joinnowbtn.isDisplayed());
  	  test.log(LogStatus.INFO, "Join now button is displayed");
  	    	
  	  joinnowbtn.click();
  	  test.log(LogStatus.INFO, "Clicked on join now button");
  	  try {
  		Thread.sleep(100);
  	  } catch (InterruptedException e) {
  		// TODO Auto-generated catch block
  		e.printStackTrace();
  	  }
        WebElement regtitle;
        regtitle = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div/div/div/strong[2]"));
    	Assert.assertTrue(regtitle.isDisplayed());
    	test.log(LogStatus.INFO, "Pop up the registartion window");
    	
    	String mm;
        String dd;
        String yy;
        mm = driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobMonth\"]")).getAttribute("value"); 
        System.out.print(mm);
        dd = driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobDay\"]")).getAttribute("value"); 
        System.out.print(dd);
        yy = driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobYear\"]")).getAttribute("value");
        System.out.print(yy);
        
        Assert.assertEquals(12, mm);
        test.log(LogStatus.INFO, "Pop up the registartion window");
        Assert.assertEquals(12, dd);
        test.log(LogStatus.INFO, "Pop up the registartion window");
        Assert.assertEquals(1988, yy);
        test.log(LogStatus.INFO, "Pop up the registartion window");
        
      }
    
      
    }
    
    
  
	

