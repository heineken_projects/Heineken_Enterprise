package testcases;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BaseTest {
	
	
    public static ExtentReports extent;
    public static ExtentTest test;
    ChromeDriver driver;
    
    @BeforeSuite
    public static ExtentReports getInstance(){
		if (extent == null )
		{
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/HeinekenReport.html", true);
		//extent.addSystemInfo("Environment","Environment Name")
		extent
                .addSystemInfo("Host Name", "SoftwareTestingMaterial")
                .addSystemInfo("Environment", "Automation Testing")
                .addSystemInfo("User Name", "Sarika S");
                extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	
		}
		return extent;
		}
    
    public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
        //after execution, you could see a folder "FailedTestsScreenshots" under src folder
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
    
    @BeforeTest
	public void beforetest()
	{	
		//System.setProperty(key, value)
		//System.setProperty("webdriver.chrome.driver","E:\\workspace\\Heineken\\asset\\chromedriver_win.exe");
		String driveloc = System.getProperty("user.dir")+"\\asset\\chromedriver_win.exe";
	    System.setProperty("webdriver.chrome.driver", driveloc);
		driver=new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	    //myWaitVar = new WebDriverWait(driver, 30);
	    
	}
    
    public void dob_checking(int mm, int dd, int yyyy)
    {
    	WebElement month,year,date,enterbtn;
    	
    	month = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	month.clear();
    	month.sendKeys(String.valueOf(mm));
        date = driver.findElement(By.xpath("//*[@id=\"day\"]"));
        date.clear();
        date.sendKeys(String.valueOf(dd));
        year = driver.findElement(By.xpath("//*[@id=\"year\"]"));
        year.clear();
        year.sendKeys(String.valueOf(yyyy)); 
        enterbtn = driver.findElement(By.xpath("//*[@id=\"input-date-submit\"]"));
        enterbtn.click();
    	System.out.println("month is"+mm+"date is:"+dd+"year is :"+yyyy);
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}							
    	
    }
    
    public void registration_BC(String email, String password, String con_password, String fname, String lname, String mm, String dd, String yyyy)
    {
    	WebElement createbtn,email_field,password_field,con_password_field,fname_field,lname_field,mm_field,dd_field,yyyy_field;
    	
    	email_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	email_field.clear();
    	email_field.sendKeys(email);
    	password_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	password_field.clear();
    	password_field.sendKeys(password);
    	con_password_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	con_password_field.clear();
    	con_password_field.sendKeys(con_password);
    	fname_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	fname_field.clear();
    	fname_field.sendKeys(fname);
    	lname_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	lname_field.clear();
    	lname_field.sendKeys(lname);
    	mm_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	mm_field.clear();
    	mm_field.sendKeys(mm);
    	dd_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	dd_field.clear();
    	dd_field.sendKeys(dd);
    	yyyy_field = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    	yyyy_field.clear();
    	yyyy_field.sendKeys(dd);
    	
    	
        createbtn = driver.findElement(By.xpath("//*[@id=\\\"input-date-submit\\\"]"));
        createbtn.click();
    	System.out.println("email is"+email+"password is:"+password+"confirm password is :"+con_password+"first name is :"+fname+"last name is :"+lname+"month is :"+mm+"date is :"+dd+"year is :"+yyyy);
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}						
    	
    }
    
    
    
    @BeforeMethod
	public void beforemethod(Method method)
	{
		
    	extent = BaseTest.getInstance();
		test = extent.startTest(method.getName());
		
	}
    
    @AfterMethod
    public void getResult(ITestResult result){
    	 if(result.getStatus() == ITestResult.FAILURE){
			test.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			test.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
			 String screenshotPath = null;
			try {
				screenshotPath = getScreenshot(driver, result.getName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				//To add it in the extent report 
			 test.log(LogStatus.FAIL, test.addScreenCapture(screenshotPath));
		}else if(result.getStatus() == ITestResult.SKIP){
			test.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
		}
		// ending test
		//endTest(logger) : It ends the current test and prepares to create HTML report
		extent.endTest(test);
	}
    @AfterTest
	public void aftertest()
	{	
    	driver.quit();	
	}
    
    @AfterSuite
    public void tearDown()
    {
        extent.flush();
        extent.close();
    }
    
    
}
