package testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

public class SignUp extends BaseTest
{
	
  @Test(priority=1)
  public void verify_AgegatePage() {
	  
	    driver.navigate().to("https://staging.hkn2.brandcommerce.io");
	    String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "got the title of agegate page");
		String exptitle ="Heineken | Age Gate";
		Assert.assertEquals(acttitle, exptitle);
		test.log(LogStatus.INFO, "verified the title of agegate page");
		System.out.println("verified agegate page title");
  }
  @Test(priority=2, dependsOnMethods="verify_AgegatePage")
  public void provide_Age_Goto_Home() {
	  
	  	test.log(LogStatus.INFO, "Providing DOB above 21 years");
  		dob_checking(12,12,1988);
  		test.log(LogStatus.INFO, "Checking whether we have reached the home page");
  		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title of home page");
		String Expectedtitle = "Heineken Store | Home";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of home page");		
		System.out.println("verified age above 21 and redirected to home page");
  }
  @Test(priority=3, dependsOnMethods= {"verify_AgegatePage","provide_Age_Goto_Home"})
  public void goto_Registartion_Window() { 
  
	  WebElement joinloginbtn,notamembertext,joinnowbtn,regi_win_title;
	  String regi_win_title_text,regi_win_title_text_expected;
	  test.log(LogStatus.INFO, "Clicked on join/Login button");
	  joinloginbtn = driver.findElement(By.xpath("/html/body/div[1]/main-navbar/div[2]/nav[1]/header/div/div/div[1]/div[2]/a"));
	  joinloginbtn.click();
	  test.log(LogStatus.INFO, "pop up the login window");
	  try {
		Thread.sleep(100);
	  		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  		}					
	  notamembertext = driver.findElement(By.cssSelector("#LoginForm > div:nth-child(6) > p:nth-child(2)"));
	  Assert.assertTrue(notamembertext.isDisplayed());
	  test.log(LogStatus.INFO, "Not a member yet text is displayed");
	  
	  joinnowbtn = driver.findElement(By.cssSelector("#LoginForm > div:nth-child(6) > a:nth-child(3)"));
	  Assert.assertTrue(joinnowbtn.isDisplayed());
	  test.log(LogStatus.INFO, "Join now button is displayed");
	    	
	  joinnowbtn.click();
	  test.log(LogStatus.INFO, "Clicked on join now button");
	  try {
		Thread.sleep(100);
	  } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	  
	  regi_win_title = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div/div/div/strong[2]"));
	  Assert.assertTrue(regi_win_title.isDisplayed());
	  test.log(LogStatus.INFO, "Registration window title is displayed");
	  regi_win_title_text = regi_win_title.getText();
	  System.out.println(regi_win_title_text);
	  regi_win_title_text_expected = "YOUR HEINEKEN NOW ACCOUNT";
	  Assert.assertEquals(regi_win_title_text, regi_win_title_text_expected);
	  test.log(LogStatus.INFO, "Compared the title of registration window");
	  System.out.println("verified the title of registration window");
  }
  @Test(priority=3, dependsOnMethods= {"verify_AgegatePage","goto_Registartion_Window"})
  public void click_verify_Minibardelivery_Link() { 
	  
	  	WebElement minibarlink;
  	
		test.log(LogStatus.INFO, "going to click on minibar delivery link");
		minibarlink = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div/div/div/form/p[2]/a[1]"));
		Assert.assertTrue(minibarlink.isDisplayed());
		minibarlink.click();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		//switch to new tab
		driver.switchTo().window(browserTabs .get(1));
		test.log(LogStatus.INFO, "switched to minibar delivery privacy policy tab");
		//check is it correct page opened or not (e.g. check page's title)
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, "getting the title of minibar delivery privacy policy tab");
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title");
		String Expectedtitle = "Privacy Policy - Minibar Delivery";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of minibar delivery privacy policy tab");
		//then close tab and get back
		driver.close();
		test.log(LogStatus.INFO, "Closed the minibar delivery privacy policy tab");
		driver.switchTo().window(browserTabs.get(0));
		test.log(LogStatus.INFO, "Redirected to registration pop up window tab again");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("redirected to registration pop up window from minibar privacy policy tab");
	  
  }
  @Test(priority=3, dependsOnMethods= {"verify_AgegatePage","goto_Registartion_Window"})
  public void click_verify_Drizly_Link() { 
	  	WebElement drizlylink;
	  	
		test.log(LogStatus.INFO, "going to click on drizly link");
		drizlylink = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div/div/div/form/p[2]/a[2]"));
		Assert.assertTrue(drizlylink.isDisplayed());
		drizlylink.click();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		//switch to new tab
		driver.switchTo().window(browserTabs .get(1));
		test.log(LogStatus.INFO, "switched to drizly privacy policy tab");
		//check is it correct page opened or not (e.g. check page's title)
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, "getting the title of drizly privacy policy tab");
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title");
		String Expectedtitle = "Drizly Legal";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of drizly privacy policy tab");
		//then close tab and get back
		driver.close();
		test.log(LogStatus.INFO, "Closed the drizly privacy policy tab");
		driver.switchTo().window(browserTabs.get(0));
		test.log(LogStatus.INFO, "Redirected to registration pop up window tab again");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("redirected to registration pop up window from drizly privacy policy tab");
	  
  }
  @Test(priority=3, dependsOnMethods= {"verify_AgegatePage","provide_Age_Goto_Home"})
  public void click_verify_TermsOfUse_Link() { 
	    WebElement termslink;
	  	
		test.log(LogStatus.INFO, "going to click on terms of use link");
		termslink = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div/div/div/form/p[2]/a[3]"));
		Assert.assertTrue(termslink.isDisplayed());
		termslink.click();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		//switch to new tab
		driver.switchTo().window(browserTabs .get(1));
		test.log(LogStatus.INFO, "switched to terms of use tab");
		//check is it correct page opened or not (e.g. check page's title)
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, "getting the title of terms of use tab");
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title");
		String Expectedtitle = "Heineken | Read the small print";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of terms of use tab");
		//then close tab and get back
		driver.close();
		test.log(LogStatus.INFO, "Closed the terms of use tab");
		driver.switchTo().window(browserTabs.get(0));
		test.log(LogStatus.INFO, "Redirected to registration pop up window tab again");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("redirected to registration pop up window from terms of use tab");
	  
	  
  }
  @Test(priority=3, dependsOnMethods= {"verify_AgegatePage","provide_Age_Goto_Home"})
  public void click_verify_PrivacyPolicy_Link() { 
	  	WebElement privacylink;
	  	
		test.log(LogStatus.INFO, "going to click on terms of use link");
		privacylink = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[1]/div/div/div/form/p[2]/a[4]"));
		Assert.assertTrue(privacylink.isDisplayed());
		privacylink.click();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		//switch to new tab
		driver.switchTo().window(browserTabs .get(1));
		test.log(LogStatus.INFO, "switched to terms of use tab");
		//check is it correct page opened or not (e.g. check page's title)
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, "getting the title of privacy policy tab");
		String acttitle = driver.getTitle();
		test.log(LogStatus.INFO, "Comparing the title");
		String Expectedtitle = "Heineken | Read the small print";
		Assert.assertEquals(acttitle, Expectedtitle);
		test.log(LogStatus.INFO, "Verified the title of privacy policy tab");
		//then close tab and get back
		driver.close();
		test.log(LogStatus.INFO, "Closed the terms of use tab");
		driver.switchTo().window(browserTabs.get(0));
		test.log(LogStatus.INFO, "Redirected to registration pop up window tab again");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("redirected to registration pop up window from terms of use tab");
		
		
		
  }
  
  
  
}
